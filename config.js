const crypto = require('crypto')
const algorithm = 'aes-256-cbc'
const password = crypto.createHash('sha256').update('dePQ8#NtBSxUcgHM').digest().slice(0,32)
const iv = new Buffer([0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0]);

var GlobalData = {
    /**
     * @param string message
     * @param array data
     * @param boolean status
     * @param int statusCode
     * @return json
     */
    respond: function(message, status = true, statusCode = 201, data){
        return {
            'status': status,
            'message': message,
            'statusCode': statusCode,
            'data': data,
        }
    },
    respondWithFailure: function(message, statusCode, data ){
        return this.respond(message, false, statusCode, data);
    },
    encrypt: function (str){
        var textbuffer = new Buffer(str);
        var cipher = crypto.createCipheriv(algorithm,password,iv);
        var crypted = Buffer.concat([cipher.update(textbuffer) , cipher.final()]);
        return crypted.toString('base64');
    },
    decrypt:function(str){
        var textBuffer = new Buffer(str, "base64");
        var decipher = crypto.createDecipheriv(algorithm,password,iv);
        var dec = Buffer.concat([decipher.update(textBuffer) , decipher.final()]);
        return dec.toString();
    },
    socketEncrypt:function(JsonObj){
        var str = JSON.stringify(JsonObj);
        return {Data:this.encrypt(str)};
    },
    socketDecrypt:function(JsonObj){
        var str = JsonObj.Data;
        return this.decryptJSON(str);
    }

}
module.exports = GlobalData;