let express = require("express")
let router =  express.Router()

const UserController = require('../controllers/user');

router.get('/getUser', UserController.getUserData);
router.get('/getUser/:userID', UserController.findUser);

module.exports = router;