let db = require('../dbconnection'); //reference of dbconnection.js
let Users = {
    GetAllUser: function(callback){
        let Query = db.query('SELECT * FROM users',[], callback);
        return Query;
    },
    findUser: function(userID,callback){
        let Query = db.query('SELECT * FROM users where id=?',[userID], callback);
        return Query;
    }
}
module.exports = Users;