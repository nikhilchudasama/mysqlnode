var UserModel = require('../models/user')
let async = require('async')
let GlobalData =  require("../config")
let Promise = require("promise")

var Users = {
    getUserData: async function(req, res, next)  {
        UserModel.GetAllUser(function(err, rows){
            if(err){
                res.send(GlobalData.respondWithFailure('something went wrong', 501, err));
            }else{
                var data = [];
                function myPromise(id){
                    return new Promise(function(resolve, reject) {
                        UserModel.GetAllUser(function (err, rows1) {
                            resolve(rows1);
                        })
                    });
                }
                async function myAsyncFunc(user) {
                    try{
                        var temp = [];
                        temp["user"] = {};
                        temp = user;
                        let result = await myPromise(user.id);
                        temp.user = result;
                        data.push(temp);
                    }
                catch(err) {
                    console.log(err);
                    }
                }
                rows.forEach(user => {
                    myAsyncFunc(user);
                });
                setTimeout(() => res.send(GlobalData.respond("get all user", true, 201, data)),5000);
                // res.send(GlobalData.respond("get all user", true, 201, data));
            }
        })

    },
    findUser: async function(req, res, next) {
        const id = req.params.userID;
        UserModel.findUser(id,function (err, rows){
            if(err){
                res.send(GlobalData.respondWithFailure('something went wrong', 501, err));
            }else{
                if(rows.length == 0){
                    res.send(GlobalData.respondWithFailure('User Not Found', 404, ''));
                }else{
                    res.send(GlobalData.respond("find User", true, 201, rows));
                }
            }
        })
    }
}
module.exports = Users