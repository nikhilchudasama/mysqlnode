var sockets = {};
var GlobalData =  require("../config");
var UserModel = require('../models/user');
var io;

sockets.init = function(server){

    // socket.io setup
    io = require('socket.io')(server, {
        // below are engine.IO options
        pingInterval: 25000,
        pingTimeout: 60000,
        cookie: false
    });

    Object.prototype.encryptData = function () {
        return GetGlobal.socketEncrypt(this)
    }

    Object.prototype.decryptData = function () {
        return GetGlobal.socketDecrypt(this)
    }
    var roomno = 1;
    io.on("connection", function(socket){

        console.log('User Connected ' + io.engine.clientsCount);

        socket.on('clientEvent', function(data, res){
            UserModel.findUser(data.userId, function(err, results){
                if(results.length == 0){
                    res(GlobalData.respondWithFailure("User Not found",404,''));
                }else{
                    res(GlobalData.respond("User Found",true, 201, results));
                }

            })
        })

        socket.join('nikhil');

        io.in('nikhil').emit('UserData', {description:'hello nikhil'});

        socket.on('disconnect', function(){
            console.log('A user disconnected');
        });
    });
}
module.exports = sockets;